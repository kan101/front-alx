import { mount } from "@vue/test-utils";
import Anime from "@/components/Anime.vue";

describe("Anime", () => {
  it("renders article for each anime in data.animeArray", () => {
    const animeArray = [{ attributes: { posterImage: "" } }];
    const wrapper = mount(Anime, {
      data() {
        return {
          animeArray,
        };
      },
    });
    expect(wrapper.findAll(".card")).toHaveLength(animeArray.length);
  });
});
