import { shallowMount } from "@vue/test-utils";
import Home from "@/views/Home.vue";
import Jokes from "@/components/Jokes.vue";
import Anime from "@/components/Anime.vue";

describe("Home", () => {

  it("renders Jokes component", () => {
    const wrapper = shallowMount(Home);
    expect(wrapper.findComponent(Jokes).exists()).toBe(true);
  });

  it("renders Anime component", () => {
    const wrapper = shallowMount(Home);
    expect(wrapper.findComponent(Anime).exists()).toBe(true);
  });
});
