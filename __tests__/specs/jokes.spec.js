import { mount } from "@vue/test-utils";
import Jokes from "@/components/Jokes.vue";

describe("Jokes", () => {
  it("renders article for each joke in data.jokes", () => {
    const jokes = ["", ""];
    const wrapper = mount(Jokes, {
      data() {
          return {
            jokes
          }
      }
    });
    expect(wrapper.findAll("article")).toHaveLength(jokes.length);
  });
});
