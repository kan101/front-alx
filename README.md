# Joke and Anime/Manga Display Application. Laravel + Vue. 

This was done for an interview. The Laravel backend is the [alx](https://gitlab.com/kan101/alx) repo in my profile. This application pulls jokes from the Official Joke API (`https://official-joke-api.appspot.com/jokes/ten`) and anime/manga data from the Kitsu API (`https://kitsu.docs.apiary.io/#introduction/json:api`). User authentication is needed for data access.

Vue.js and Bootstrap Vue are used on the frontend for interface construction and layout, respectively. SCSS is applied for advanced styling capabilities.

The backend employs Laravel, a PHP framework, and data is stored and managed using MySQL.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
