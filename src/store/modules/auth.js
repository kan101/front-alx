import axios from "../../axios.config";

const state = {
  token: localStorage.getItem("token") || null,
};

const actions = {
  register({ commit }, data) {
    commit("registerRequest");
    let url = `/api/user/register`;
    return axios
      .post(url, data)
      .then(function(response) {
        commit("registerSuccess", response.data);
        return response;
      })
      .catch(function(error) {
        commit("registerFailure", error);
        throw error;
      });
  },

  login({ commit }, data) {
    commit("loginRequest");
    let url = `/api/user/login`;
    return axios
      .post(url, data)
      .then(function(response) {
        commit("loginSuccess", response.data);
        return response;
      })
      .catch(function(error) {
        commit("loginFailure", error);
        throw error;
      });
  },

  logout({ commit }) {
    commit("logoutRequest");
    let url = `/api/user/logout`;
    return axios
      .post(url)
      .then(function(response) {
        commit("logoutSuccess", response.data);
        return response;
      })
      .catch(function(error) {
        commit("logoutFailure", error);
        throw error;
      });
  },

  logoutWithoutPost({ commit }) {
    commit("logoutSuccess");
  }
};

const mutations = {
  loginRequest() {},

  loginSuccess(state, data) {
    localStorage.setItem("token", data.token);
    state.token = data.token;
  },
  loginFailure(state) {
    state.token = "";
  },
  registerRequest() {},

  registerSuccess(state, data) {
    state.token = data.results;
  },
  registerFailure(state) {
    state.token = "";
  },
  logoutRequest() {},

  logoutSuccess(state) {
    localStorage.clear();
    state.token = null;
  },
  logoutFailure() {
    //state.token = "";
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
