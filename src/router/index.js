import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "*",
    redirect: "/login",
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if (
    ((to && to.name === "Login") || (to && to.name === "Register")) &&
    (store.state.auth.token != null )
  ) {
    return next({ name: "Home" });
  }

  if (
    to &&
    to.name !== "Login" &&
    to &&
    to.name !== "Register" &&
    store.state.auth.token === null
  ) {
    return next({ name: "Login" });
  }
  next();
});

export default router;
