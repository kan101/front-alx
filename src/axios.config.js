import axios from "axios";
import store from "./store/index";
import router from "./router";

const instance = axios.create({
  baseURL: "https://test.detimi.com"
});

instance.interceptors.request.use(config => {
  if (store.state.auth.token) {
    config.headers.Authorization = `Bearer ${store.state.auth.token}`;
  }
  return config;
});

instance.interceptors.response.use(
  response => response,
  error => {
    if (
      error.response.status == 401
    ) {
      store
        .dispatch("auth/logoutWithoutPost")
        .then(() => {
          router.push({
            name: "login"
          });
        })
        .catch(() => {
          router.push({
            name: "login"
          });
        });
    }
  }
);

export default instance;
